import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    token:localStorage.getItem('token')||'',
    username:localStorage.getItem('username')||''
  },
  mutations: {
    setData:(state,payload) => {
      state.token=payload.token;
      localStorage.setItem('token',payload.token);
      state.username=payload.username;
      localStorage.setItem('username',payload.username);
    },
    initState:(state) => {
      state.token='';
      state.username='';
      localStorage.removeItem('token');
      localStorage.removeItem('username');
    }
  },
  actions: {
  },
  modules: {
  }
})
