import {http} from '@/http'
const myPlugin={
    install:(Vue,options)=>{
        Vue.prototype.http=(...obj)=>{
            return http(...obj);
        }
    }
}
export default myPlugin