import { http } from './index'
import { Message } from 'element-ui'

// 获取菜单权限列表
export function getMenus() {
    return http('menus', 'GET').then(res => {
        return res.data;
    })
}

// 获取用户列表
export function getUsers(params) {
    return http('users', 'GET', {}, params).then(res => {
        return res.data
    })
}

//添加用户
export function addUsers(data) {
    return http('users', 'POST', data).then(res => {
        Message({
            type: "success",
            message: res.meta.msg
        })
        return res.data
    })
}

// 更新用户状态
export function updateUser(uId, type) {
    return http(`users/${uId}/state/${type}`, 'PUT').then(res => {
        Message({
            type: "success",
            message: res.meta.msg
        })
        return res.data
    })
}

//编辑用户信息
export function editUser(obj) {
    return http(`users/${obj.id}`, 'PUT', {
        email: obj.email,
        mobile: obj.mobile
    }).then(res => {
        Message({
            type: "success",
            message: res.meta.msg
        })
        return res.data
    })
}

//删除用户
export function rmUser(id) {
    return http(`users/${id}`, 'DELETE').then(res => {
        Message({
            type: "success",
            message: res.meta.msg
        })
        return res.data
    })
}

// 根据用户id查询角色
export function queryUser(id) {
    return http(`users/${id}`, 'GET').then(res => {
        return res.data
    })
}

//获取角色列表
export function getRoleList() {
    return http('roles', 'GET').then(res => {
        return res.data
    })
}

// 编辑提交角色
export function editAllot(id, rid) {
    return http(`users/${id}/role`, 'PUT', {
        rid
    }).then(res => {
        Message({
            type: "success",
            message: res.meta.msg
        })
        return res.data
    })
}

// 添加角色
export function addRoles(roleName, roleDesc) {
    return http('roles', 'POST', {
        roleName, roleDesc
    }).then(res => {
        Message({
            type: "success",
            message: res.meta.msg
        })
        return res.data
    })
}

// 删除角色指定权限
export function rmRolesRights(roleId, rightId) {
    return http(`roles/${roleId}}/rights/${rightId}`, 'DELETE').then(res => {
        Message({
            type: "success",
            message: res.meta.msg
        })
        return res.data
    })
}

// 编辑提交角色
export function editRoles(data) {
    return http(`roles/${data.id}`, 'PUT', {
        roleName: data.roleName,
        roleDesc: data.roleDesc
    }).then(res => {
        Message({
            type: "success",
            message: res.meta.msg
        })
        return res.data
    })
}

// 删除角色
export function rmRoles(id) {
    return http(`roles/${id}`, 'DELETE').then(res => {
        Message({
            type: "success",
            message: res.meta.msg
        })
        return res.data
    })
}

// 权限列表
export function rightsList(type) {
    return http(`rights/${type}`, 'GET').then(res => {
        return res.data
    })
}

// 角色授权
export function rolesAuth(roleId,rids) {
    return http(`roles/${roleId}/rights`, 'POST', {
        rids
    }).then(res => {
        Message({
            type: "success",
            message: res.meta.msg
        })
        return res.data
    })
}

// 商品列表
export function getGoodsList(params){
    return http('goods','GET',{},params).then( res => {
        return res.data
    })
}

// 添加商品
// 获取商品分类
export function goodsCategories(){
    return http('categories','GET',[1,2,3]).then( res => {
        return  res.data
    })
}

// 获取商品参数
export function goodsAttrs(id,sel){
    return http(`categories/${id}/attributes`,'GET',{},{sel}).then( res => {
        return res.data
    })
}

// 删除商品分类
export function rovemeGoods(id) {
    return http(`goods/${id}`,'DELETE').then( res => {
        Message({
            type: "success",
            message: res.meta.msg
        })
        return res.data
    })
  }

// 添加商品
export function addGoods(data) {
    return http("goods",'POST',data).then( res => {
        Message({
            type: "success",
            message: res.meta.msg
        })
        return res.data
    })
  }