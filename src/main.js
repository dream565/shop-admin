import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import 'reset-css'
import myPlugin from './plugin'
import dayjs from 'dayjs'
import VueQuillEditor from 'vue-quill-editor'
import 'quill/dist/quill.core.css' // import styles
import 'quill/dist/quill.snow.css' // for snow theme
import 'quill/dist/quill.bubble.css' // for bubble theme
Vue.use(VueQuillEditor, /* { default global options } */)
Vue.use(myPlugin)
Vue.use(ElementUI);
Vue.config.productionTip = false
Vue.filter('dealDate',(val) => {
  return dayjs(val).format('YYYY-MM-DD HH:mm:ss');
})


new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
