import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../views/login.vue'
import store from '../store/index'
Vue.use(VueRouter)

  const routes = [
    {
      path:"/",
      redirect:"/login",
      meta:{
        notAuthorization:true
      }
    },
    {
      path:"/login",
      name:'login',
      component:Login
    },
    {
      path:"/index",
      name:'index',
      redirect:"/index/users",
      component:() => import('../views/Index.vue'),
      children:[
        {
          path:"users",
          name:"users",
          component:() => import('../views/Users.vue'),
        },
        {
          path:"roles",
          name:"roles",
          component:() => import('../views/rights/Roles.vue'),
        },
        {
          path:"rights",
          name:"rights",
          component:() => import('../views/rights/Rights.vue'),
        },
        {
          path:"goods",
          name:"goods",
          component:() => import('../views/goods/Goods.vue'),
        },
        {
          path:"addgoods",
          name:"addgoods",
          component:() => import('../views/goods/AddGoods.vue'),
        },
      ]
    },
]

const router = new VueRouter({
  routes
})

router.beforeEach((to,from,next) => {
  if(!to.meta.notAuthorization){
    if(store.state.token){
      next()
    }else{
      if(to.path=='/login'){
        next();
      }else{
        next({
          path:"/login"
        })
      }
    }
  }else{
    next();
  }
})

export default router
